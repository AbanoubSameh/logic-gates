package logic;

public class AdvancedFourBitAdder {
	
	private String sum0;
	private String sum1;
	private String sum2;
	private String sum3;
	
	private String carry;
	
	public AdvancedFourBitAdder(int a3, int a2, int a1, int a0, int b3, int b2, int b1, int b0) {
		
		OneBitAdder adder0 = new OneBitAdder(a0, b0);
		
		FullOneBitAdder adder1 = new FullOneBitAdder(adder0.getCarry(), a1, b1);
		
		FullOneBitAdder adder2 = new FullOneBitAdder(adder1.getCarryOut(), a2, b2);
		
		FullOneBitAdder adder3 = new FullOneBitAdder(adder2.getCarryOut(), a3, b3);
		
		this.sum0 = String.valueOf(adder0.getSum());
		this.sum1 = String.valueOf(adder1.getSum());
		this.sum2 = String.valueOf(adder2.getSum());
		this.sum3 = String.valueOf(adder3.getSum());
		
		this.carry = String.valueOf(adder3.getCarryOut());
		
	}
	
	public int getSum0() {
		return Integer.parseInt(sum0);
	}
	
	public int getSum1() {
		return Integer.parseInt(sum1);
	}
	
	public int getSum2() {
		return Integer.parseInt(sum2);
	}
	
	public int getSum3() {
		return Integer.parseInt(sum3);
	}
	
	public int getCarry() {
		return Integer.parseInt(carry);
	}

}
