package logic;

import logic.LogicGates.*;

public class FourBitAdder {
	
	private String sum0;
	private String sum1;
	private String sum2;
	private String sum3;
	
	private String carry;
	
	public FourBitAdder(int a3, int a2, int a1, int a0, int b3, int b2, int b1, int b0) {
		
		xorGate sum0 = new xorGate(a0, b0);
		andGate carry0 = new andGate(a0, b0);
		
		xorGate axb1 = new xorGate(a1, b1);
		andGate ab1 = new andGate(a1, b1);
		andGate ab11 = new andGate(axb1.getY(), carry0.getY());
		
		xorGate sum1 = new xorGate(axb1.getY(), carry0.getY());
		orGate carry1 = new orGate(ab11.getY(),  ab1.getY());
		
		
		xorGate axb2 = new xorGate(a2, b2);
		andGate ab2 = new andGate(a2, b2);
		andGate ab22 = new andGate(axb2.getY(), carry1.getY());
		
		xorGate sum2 = new xorGate(axb2.getY(), carry1.getY());
		orGate carry2 = new orGate(ab22.getY(),  ab2.getY());
		
		xorGate axb3 = new xorGate(a3, b3);
		andGate ab3 = new andGate(a3, b3);
		andGate ab33 = new andGate(axb3.getY(), carry2.getY());
		
		xorGate sum3 = new xorGate(axb3.getY(), carry2.getY());
		orGate carry3 = new orGate(ab33.getY(),  ab3.getY());
		
		
		this.sum0 = String.valueOf(sum0.getY());
		this.sum1 = String.valueOf(sum1.getY());
		this.sum2 = String.valueOf(sum2.getY());
		this.sum3 = String.valueOf(sum3.getY());
		
		this.carry = String.valueOf(carry3.getY());
		
	}
	
	public int getSum0() {
		return Integer.parseInt(sum0);
	}
	
	public int getSum1() {
		return Integer.parseInt(sum1);
	}
	
	public int getSum2() {
		return Integer.parseInt(sum2);
	}
	
	public int getSum3() {
		return Integer.parseInt(sum3);
	}
	
	public int getCarry() {
		return Integer.parseInt(carry);
	}
	
}
