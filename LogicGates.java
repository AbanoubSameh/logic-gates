package logic;

public abstract class LogicGates {

	public static class andGate {

		private String y;

		public andGate(int a, int b) {

			if ((a == 0 && b == 0) || (a == 1 && b == 0) || (a == 0 && b == 1)) {
				this.y = "0";
			} else if (a == 1 && b == 1) {
				this.y = "1";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class orGate {

		private String y;

		public orGate(int a, int b) {

			if ((a == 0 && b == 0)) {
				this.y = "0";
			} else if ((a == 1 && b == 0) || (a == 0 && b == 1) || (a == 1 && b == 1)) {
				this.y = "1";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class xorGate {

		private String y;

		public xorGate(int a, int b) {

			if ((a == 0 && b == 0) || (a == 1 && b == 1)) {
				this.y = "0";
			} else if ((a == 0 && b == 1) || (a == 1 && b == 0)) {
				this.y = "1";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class notGate {

		private String y;

		public notGate(int a) {

			if (a == 0) {
				this.y = "1";
			} else if (a == 1) {
				this.y = "0";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class nandGate {

		private String y;

		public nandGate(int a, int b) {

			if ((a == 0 && b == 0) || (a == 1 && b == 0) || (a == 0 && b == 1)) {
				this.y = "1";
			} else if (a == 1 && b == 1) {
				this.y = "0";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class norGate {

		private String y;

		public norGate(int a, int b) {

			if ((a == 0 && b == 0)) {
				this.y = "1";
			} else if ((a == 1 && b == 0) || (a == 0 && b == 1) || (a == 1 && b == 1)) {
				this.y = "0";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class xnorGate {

		private String y;

		public xnorGate(int a, int b) {

			if ((a == 0 && b == 0) || (a == 1 && b == 1)) {
				this.y = "1";
			} else if ((a == 0 && b == 1) || (a == 1 && b == 0)) {
				this.y = "0";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

}
