package logic;

public class EightBitAdder {
	
	private String sum0;
	private String sum1;
	private String sum2;
	private String sum3;
	private String sum4;
	private String sum5;
	private String sum6;
	private String sum7;
	
	private String carry;

	public EightBitAdder(int a7, int a6, int a5, int a4, int a3, int a2, int a1, int a0, int b7, int b6, int b5, int b4,
			int b3, int b2, int b1, int b0) {

		OneBitAdder adder0 = new OneBitAdder(a0, b0);

		FullOneBitAdder adder1 = new FullOneBitAdder(adder0.getCarry(), a1, b1);

		FullOneBitAdder adder2 = new FullOneBitAdder(adder1.getCarryOut(), a2, b2);

		FullOneBitAdder adder3 = new FullOneBitAdder(adder2.getCarryOut(), a3, b3);
		
		FullOneBitAdder adder4 = new FullOneBitAdder(adder3.getCarryOut(), a4, b4);

		FullOneBitAdder adder5 = new FullOneBitAdder(adder4.getCarryOut(), a5, b5);

		FullOneBitAdder adder6 = new FullOneBitAdder(adder5.getCarryOut(), a6, b6);
		
		FullOneBitAdder adder7 = new FullOneBitAdder(adder6.getCarryOut(), a7, b7);
		
		this.sum0 = String.valueOf(adder0.getSum());
		this.sum1 = String.valueOf(adder1.getSum());
		this.sum2 = String.valueOf(adder2.getSum());
		this.sum3 = String.valueOf(adder3.getSum());
		this.sum4 = String.valueOf(adder4.getSum());
		this.sum5 = String.valueOf(adder5.getSum());
		this.sum6 = String.valueOf(adder6.getSum());
		this.sum7 = String.valueOf(adder7.getSum());
		
		this.carry = String.valueOf(adder7.getCarryOut());

	}
	
	public int getSum0() {
		return Integer.parseInt(sum0);
	}
	
	public int getSum1() {
		return Integer.parseInt(sum1);
	}
	
	public int getSum2() {
		return Integer.parseInt(sum2);
	}
	
	public int getSum3() {
		return Integer.parseInt(sum3);
	}
	
	public int getSum4() {
		return Integer.parseInt(sum4);
	}
	
	public int getSum5() {
		return Integer.parseInt(sum5);
	}
	
	public int getSum6() {
		return Integer.parseInt(sum6);
	}
	
	public int getSum7() {
		return Integer.parseInt(sum7);
	}
	
	public int getCarry() {
		return Integer.parseInt(carry);
	}

}
