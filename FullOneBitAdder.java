package logic;

import logic.LogicGates.andGate;
import logic.LogicGates.xorGate;
import logic.LogicGates.orGate;

public class FullOneBitAdder {

	private String carryOut;
	private String sum;

	public FullOneBitAdder(int carryIn, int a, int b) {

		xorGate axb = new xorGate(a, b);
		andGate ab = new andGate(a, b);
		andGate ab1 = new andGate(axb.getY(), carryIn);

		xorGate sum = new xorGate(axb.getY(), carryIn);
		orGate carryOut = new orGate(ab1.getY(), ab.getY());

		this.carryOut = String.valueOf(carryOut.getY());
		this.sum = String.valueOf(sum.getY());

	}
	
	public int getSum() {
		return Integer.parseInt(sum);
	}
	
	public int getCarryOut() {
		return Integer.parseInt(carryOut);
	}

}
