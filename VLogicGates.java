package logic;

public abstract class VLogicGates {

	public static class vandGate {

		private String y;

		public vandGate(int[] x) {

			char ret = '1';

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 0) {
					ret = '0';
				}
			}

			if (ret == '0') {
				this.y = "0";
			} else if (ret == '1') {
				this.y = "1";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class vorGate {

		private String y;

		public vorGate(int[] x) {

			char ret = '0';

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 1) {
					ret = '1';
				}
			}

			if (ret == '0') {
				this.y = "0";
			} else if (ret == '1') {
				this.y = "1";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class vxorGate {

		private String y;

		public vxorGate(int[] x) {

			int ret = 0;

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 1) {
					ret++;
				}
			}

			if (ret % 2 == 0 || ret == 0) {
				this.y = "0";
			} else if (ret % 2 != 0) {
				this.y = "1";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class vnandGate {

		private String y;

		public vnandGate(int[] x) {

			char ret = '1';

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 0) {
					ret = '0';
				}
			}

			if (ret == '0') {
				this.y = "1";
			} else if (ret == '1') {
				this.y = "0";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class vnorGate {

		private String y;

		public vnorGate(int[] x) {

			char ret = '0';

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 1) {
					ret = '1';
				}
			}

			if (ret == '0') {
				this.y = "1";
			} else if (ret == '1') {
				this.y = "0";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

	public static class vxnorGate {

		private String y;

		public vxnorGate(int[] x) {

			int ret = 0;

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 1) {
					ret++;
				}
			}

			if (ret % 2 == 0 || ret == 0) {
				this.y = "1";
			} else if (ret % 2 != 0) {
				this.y = "0";
			}

		}

		public int getY() {
			return Integer.parseInt(y);
		}

	}

}
