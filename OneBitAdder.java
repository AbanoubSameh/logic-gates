package logic;

import logic.LogicGates.*;

public class OneBitAdder {
	
	private String sum;
	private String carry;
	
	public OneBitAdder(int a, int b) {
		
		xorGate sum = new xorGate(a, b);
		andGate carry = new andGate(a, b);
		
		this.sum = String.valueOf(sum.getY());
		this.carry = String.valueOf(carry.getY());
		
	}
	
	public int getSum() {
		return Integer.parseInt(sum);
	}
	public int getCarry() {
		return Integer.parseInt(carry);
	}

}
